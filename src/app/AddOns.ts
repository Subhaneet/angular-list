export class AddOns {
  private title: string;
  private addNew: boolean;

  constructor(title?: string, addNew?: boolean) {
    this.title = title;
    this.addNew = addNew;
  }
}
