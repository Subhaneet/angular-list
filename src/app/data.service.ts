import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  todos: any[] = [
    {
      title: 'Emergency',
      isChecked: false,
      addOns: [
        {
          title: '',
          color: false,
          addNew: false
        }
      ]
    },
    {
      title: 'B',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'C',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'C',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'D',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'E',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'F',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'G',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'H',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    },
    {
      title: 'I',
      isChecked: false,
      addOns: [
        {
          title: '',
          addNew: false
        }
      ]
    }
  ];
  constructor() { }

  getTreatment() {
    return this.todos;
  }
}
