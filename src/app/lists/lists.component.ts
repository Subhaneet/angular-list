import { AddOns } from './../AddOns';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  constructor(private dataService: DataService) {}

  todos: any[] = this.dataService.getTreatment();
  toggle = true;
  status = 'Enable';

  checkBox(data) {
    data.isChecked = ! data.isChecked;
  }

  addNew(item) {
    const newItem = new AddOns();
    item.addOns.push(newItem);
  }

  changeColor() {
    this.toggle = !this.toggle;
    this.status = this.toggle ? 'red' : 'green';
  }

  ngOnInit() {}
}
